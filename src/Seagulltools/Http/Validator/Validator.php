<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-02-28
 * Time: 18:06
 */

namespace Seagulltools\Http\Validator;

use Seagulltools\Http\Exception\MethodValidationException;
use Seagulltools\Http\Exception\UrlValidationException;

trait Validator
{
    private $customHeaders = [];

    public function addCustomHeader($key, $value)
    {
        $this->customHeaders[$key] = $value;
    }

    public function addCustomHeaders($headers)
    {
        foreach ($headers as $key => $value) {
            $this->addCustomHeader($key, $value);
        }
    }

    public function validate()
    {
        $this->validateMethod();

        $this->validateUrl();

        $this->validateHeaders();
    }

    protected function validateUrl()
    {
        if(empty($this->url)) {
            throw new UrlValidationException( 'url attribute must be valued');
        }

        if(!preg_match('/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/', $this->url))
        {
            throw new UrlValidationException( 'url attribute is not a valid url');
        }
    }

    protected function validateMethod()
    {
        $this->method = strtoupper($this->method);
        if (empty($this->method)) {
            throw new MethodValidationException('verb attribute must be valued');
        }

        if (!in_array($this->method ,Method::all())) {
            throw new MethodValidationException('verb attribute is not valid.');
        }
    }

    protected function validateHeaders()
    {
        if(!empty($this->headers)) {
            foreach ($this->headers as $header => $values) {
                if (!in_array($header, array_merge(Header::all(), $this->customHeaders))) {
                    throw new \Exception($header . ' is not a valid http header');
                }
            }
        }
    }
}