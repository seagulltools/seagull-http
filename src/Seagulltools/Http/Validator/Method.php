<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-02-28
 * Time: 18:25
 */

namespace Seagulltools\Http\Validator;

class Method
{
    const METHOD = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
        'HEAD',
        'PATCH'
    ];

    public static function all()
    {
        return self::METHOD;
    }
}