<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-02-28
 * Time: 20:00
 */

namespace Seagulltools\Http;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Seagulltools\Http\Exception\ApiException;
use Seagulltools\Http\Validator\Validator;

class Client
{
    use Validator;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $method;

    /**
     * @var mixed
     */
    private $query;

    /**
     * @var mixed
     */
    private $headers;

    /**
     * @var mixed
     */
    private $body;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var GuzzleClient
     */
    private $client;

    /**
     * @var
     */
    private $response;

    /**
     * @var
     */
    private $statusCode;

    public $stringResponse = false;

    public function __construct()
    {
        $this->client = new GuzzleClient();
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function header(string $key, $value)
    {
        $this->headers[$key] = $value;
    }

    /**
     * @param array $headers
     */
    public function headers(array $headers)
    {
        foreach ($headers as $key => $value) {
            $this->header($key, $value);
        }
    }

    /**
     * @param $param
     * @param $value
     */
    public function query($param , $value)
    {
        $this->query[$param] = $value;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->body[$name] = $value;
    }

    public function sendList($list)
    {
        $this->body = $list;

        return $this;
    }

    public function send()
    {
        $this->validate();

        $this->setOptions();

        try {
            $this->client = $this->client->request(
                $this->method,
                $this->url,
                $this->options
            );

            $this->statusCode = $this->client->getStatusCode();
            $this->response = $this->response();

            return $this;
        }
        catch (RequestException $e) {
            return $this->requestException($e);
        }
        catch (\Exception $e) {
            throw new ApiException($e->getCode() . ' - ' . $e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed
     */
    private function response()
    {
        $contentType = $this->client->getHeader('content-type')[0] ?? null;

        if($this->stringResponse) {
            return (string) $this->client->getBody();
        }

        if(preg_match('/application\/json/', $contentType)) {
            return \GuzzleHttp\json_decode($this->client->getBody());
        }

        if(preg_match('/application\/rss\+xml/', $contentType)) {
            return simplexml_load_string($this->client->getBody());
        }

        return $this->client->getBody();
    }

    /**
     *
     */
    private function setOptions()
    {
        //SET HEADERS
        if ($this->headers) {
            $this->options['headers'] = $this->headers;
        }

        //SET QUERY
        if ($this->query) {
            $this->options['query'] = $this->query;
        }

        //SET BODY
        if (isset($this->headers['Content-Type'])) {
            switch ($this->headers['Content-Type']) {
                case 'application/json':
                    $this->options['body'] = json_encode($this->body);
                    break;

                case 'multipart/form-data':
                    foreach ($this->body as $field => $value) {
                        $data[] = [
                            'name' => $field,
                            'contents' => $value
                        ];
                    }

                    $this->options['multipart'] = $data;

                    break;

                case 'application/x-www-form-urlencoded':
                    $this->options['form_params'] = $this->body;

                    break;

                default:
                    $this->options['form_params'] = $this->body;

                    break;
            }
        } else {
            $this->options['form_params'] = $this->body;
        }
    }

    /**
     * @param RequestException $e
     * @return mixed
     * @throws ApiException
     */
    private function requestException(RequestException $e)
    {
        $this->statusCode = $e->getCode();

        switch ($e->getCode()) {
            case 404:
                throw new ApiException('[HTTP][' . $this->method . '][404] - Not Found! URL:' . $this->url);
                break;

            case 400:
                $this->response = $e->getMessage();
                break;
            case 422:
                $this->response = \json_decode($e->getResponse()->getBody());
                break;

            case 503:
                throw new ApiException('[HTTP][' . $this->method . '][503] - Service Unavailable! URL:' . $this->url);
                break;

            default:
                throw new ApiException( '[HTTP][' . $this->method . '][' . $e->getCode() . '] - ' . $e->getMessage());
                break;
        }

        return $this;
    }
}
