#Seagull HTTP

###Installation
`composer require seagulltools/seagull-http`

###Usage
```php
use Seagulltools\Http\Client;

class ClassName 
{
    $client = new Client();
    
    $client->url = 'url';
    $client->method = 'post';
}
```

####Custom Headers
```php
//set single custom header
$client->addCustomHeader('custom-header-name', 'custom-header');

//set multiple headers 
$headers = [
   'custom-header-name-2' => 'custom-header-value-1',
   'custom-header-name-2' => 'custom-header-value-2'
];

$client->addCustomHeaders($headers);
```

####Set Headers
```php
//set single header
$client->header($key, $value);

//set multiple headers 
$headers = [
   'Accept' => 'application/json',
   'Content' => 'application/json'
];

$client->headers($headers);
```

####Set Body
```php
$client->name = 'John';
$client->surname = 'Doe';
$client->phone = '1-541-754-3010';
```

####Send and Get Response
```php
$client->send();

//get response
$client->getResponse();

//get status code
$client->getStatusCode();
```

####Multipart Form
```php
$client->multipart = true;
```
